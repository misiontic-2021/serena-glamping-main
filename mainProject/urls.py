from django.contrib import admin
from django.urls import path

from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)

# Project views imports
from mainApp import views

# Project routes: add paths here with path('/api/[MY_PATH]', views.[VIEW_CLASS].as_view()),
urlpatterns = [
    path('admin/', admin.site.urls),
    # User
    path('api/v1/users/', views.UserView.as_view()),
    path('api/v1/users/login/', TokenObtainPairView.as_view()),
    path('api/v1/users/refresh/', TokenRefreshView.as_view()),
    # Glamping
    path('api/v1/glampings/', views.GlampingView.as_view()),
    path('api/v1/glampings/filter/', views.GlampingFiltersView.as_view()),
    # Reservation
    path('api/v1/booking/', views.ReservaView.as_view()),
]
