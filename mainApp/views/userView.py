from rest_framework import views, status
from rest_framework.response import Response
from django.conf import settings
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend

from mainApp.models.userModel import User
from mainApp.serializers.userSerializer import UserSerializer


class UserView(views.APIView):

    def post(self, request, *args, **kwargs):
        """
        Creates a new User object (and stores it on database as a new row) with the given data request.body.
        Logs the new User in and returns the authentication tokens.
        """
        body = request.data
        # Instance a new object given a json
        serializer = UserSerializer(data=body)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        # Generate token
        tokenData = {
            "username": request.data["username"],
            "password": request.data["password"],
        }
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)
        # Send response
        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):
        """
        Retrieves all users from the database as a JSON.
        Only superusers are allowed to do this.
        """
        # Verify authentication
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        # Check if is superuser
        userObj = User.objects.get(id=valid_data['user_id'])
        if not userObj.is_superuser:
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'Your user role does not have authorization to do that'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Read all instances
        queryset = User.objects.all()
        # Serialize to send
        userSer = UserSerializer(instance=queryset, many=True)
        return Response(userSer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        """
        Updates any allowed user field given the user id (which is retrieved from token data).
        """
        body = request.data
        # Verify authentication
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        # Retrieve user by id
        userObj = User.objects.get(id=valid_data['user_id'])
        # Update user given fields
        #FIXME allows update phone with initial number != 3
        for attribute in body:
            setattr(userObj, attribute, body[attribute])
        userObj.save()
        # Serialize to send
        userSer = UserSerializer(instance=userObj)
        return Response(userSer.data, status=status.HTTP_200_OK)
