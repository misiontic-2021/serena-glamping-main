from rest_framework import views, status
from rest_framework.response import Response
from django.conf import settings
from rest_framework_simplejwt.backends import TokenBackend

from mainApp.models.userModel import User
from mainApp.models.glampingModel import Glamping
from mainApp.serializers.glampingSerializer import GlampingSerializer


class GlampingView(views.APIView):

    def post(self, request, *args, **kwargs):
        """
        Creates a new Glamping object (and stores it on database as a new row) with the given data request.body
        """
        body = request.data
        # Verify a token exists
        if not request.META.get('HTTP_AUTHORIZATION'):
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'You must be logged in to create a new glamping'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Verify authentication
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        # Check if is superuser
        userObj = User.objects.get(id=valid_data['user_id'])
        if not userObj.is_superuser:
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'Your user role does not have authorization to do that'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Instance a new object given a json
        glampingObj = GlampingSerializer(data=body)
        glampingObj.is_valid(raise_exception=True)
        glampingObj.save()
        return Response({'status': "OK"}, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):
        """
        Retrieves all glampings from the database as a JSON
        """
        # Read all instances
        queryset = Glamping.objects.all()
        # Serialize to send
        glampingsSer = GlampingSerializer(instance=queryset, many=True)
        return Response(glampingsSer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        """
        Updates any glamping field given the glamping id.
        ID is a query param and the new info is in request.data
        """
        params = dict(request.query_params)
        body = request.data
        # Verify a token exists
        if not request.META.get('HTTP_AUTHORIZATION'):
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'You must be logged in to update glamping info'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Verify authentication
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        # Check if is superuser
        userObj = User.objects.get(id=valid_data['user_id'])
        if not userObj.is_superuser:
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'Your user role does not have authorization to do that'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Get glamping by id
        glampingObj = Glamping.objects.get(id=params['id'][0])
        # Update glamping
        for attribute in body:
            setattr(glampingObj, attribute, body[attribute])
        glampingObj.save()
        # Serialize to send
        glampingSer = GlampingSerializer(instance=glampingObj)
        return Response(glampingSer.data, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        """
        Deletes a glamping given its id as a query param
        """
        params = dict(request.query_params)
        # Verify a token exists
        if not request.META.get('HTTP_AUTHORIZATION'):
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'You must be logged in to delete a glamping'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Verify authentication
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        # Check if is superuser
        userObj = User.objects.get(id=valid_data['user_id'])
        if not userObj.is_superuser:
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'Your user role does not have authorization to do that'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Get glamping by id
        glampingObj = Glamping.objects.get(id=params['id'][0])
        # Delete glamping
        glampingObj.delete()
        return Response({'status': "DELETED", 'id': params['id'][0]}, status=status.HTTP_200_OK)
