from datetime import datetime
from rest_framework import views, status
from rest_framework.response import Response
from django.conf import settings
from rest_framework_simplejwt.backends import TokenBackend

from mainApp.models.reservaModel import Reserva
from mainApp.serializers.reservaSerializer import ReservaSerializer


class ReservaView(views.APIView):

    def post(self, request, *args, **kwargs):
        """
        Creates a new booking with the given data request.body.
        The user_id field is retrieved from token decoded data.
        """
        body = request.data
        # Verify a token exists
        if not request.META.get('HTTP_AUTHORIZATION'):
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'You must be logged in to create a new booking'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Verify authentication
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        # Add user id to request
        body['user'] = valid_data['user_id']
        # Instance a new object given a json
        reserveObj = ReservaSerializer(data=body)
        reserveObj.is_valid(raise_exception=True)
        reserveObj.save()
        return Response({'status': "OK"}, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):
        """
        Retrieves all bookings from the database as a JSON given a id_user
        """
        # Verify a token exists
        if not request.META.get('HTTP_AUTHORIZATION'):
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'You must be logged in to get your bookings'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Verify authentication
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
      # Read all instances
        queryset = Reserva.objects.filter(user=valid_data['user_id'])
        # Serialize to send
        reservasSer = ReservaSerializer(instance=queryset, many=True)
        return Response(reservasSer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        """
        Updates these booking fields (checkin, checkout, people) given the reserva id.
        ID reserve is a query param and the new info is in request.data
        """
        params = dict(request.query_params)
        body = request.data
        # Verify a token exists
        if not request.META.get('HTTP_AUTHORIZATION'):
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'You must be logged in to update a booking'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Verify authentication
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        print(valid_data)
        # Get reserva by id
        reservaObj = Reserva.objects.get(id=params['id'][0])
        # Verify authenticated used in booking
        if reservaObj.user.id != valid_data['user_id']:
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'The authenticated user does not match with the booking user'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Verify the booking is not canceled
        if reservaObj.status == 'D':
            stringResponse = {
                'Error': 'Forbidden booking update',
                'detail': 'You can\'t uptate a canceled booking'
            }
            return Response(stringResponse, status=status.HTTP_403_FORBIDDEN)
        # Update reserva
        for attribute in body:
            setattr(reservaObj, attribute, body[attribute])
        reservaObj.updated_at = datetime.now()
        reservaObj.save()
        # Serialize to send
        reservaSer = ReservaSerializer(instance=reservaObj)
        return Response(reservaSer.data, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        """
        Changes the booking status to CANCELED given reserve_id as a query param
        """
        params = dict(request.query_params)
        # Verify a token exists
        if not request.META.get('HTTP_AUTHORIZATION'):
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'You must be logged in to cancel a booking'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Verify authentication
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        # Get reserva by id
        reservaObj = Reserva.objects.get(id=params['id'][0])
        # Verify authenticated used in booking
        if reservaObj.user.id != valid_data['user_id']:
            stringResponse = {
                'Error': 'Unauthorized Request',
                'detail': 'The authenticated user does not match with the booking user'
            }
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # Cancel booking
        reservaObj.status = 'D'
        reservaObj.updated_at = datetime.now()
        reservaObj.save()
        # Serialize to send
        glampingSer = ReservaSerializer(instance=reservaObj)
        return Response(glampingSer.data, status=status.HTTP_200_OK)
