from rest_framework import views, status
from rest_framework.response import Response

from mainApp.models.glampingModel import Glamping
from mainApp.serializers.glampingSerializer import GlampingSerializer

class GlampingFiltersView(views.APIView):

    def get(self, request, *args, **kwargs):
        """
        Redirect to filter function according to query params list
        Sends the response.
        """
        params = dict(request.query_params)
        # Select filter
        if 'includes' in params.keys():
            queryset = self.filterByIncludes(request)
        elif 'price_min' in params.keys() or 'price_max' in params.keys():
            queryset = self.filterByPrice(request)
        else:
            raise ValueError("The filter does not exists")
        # Serialize to send
        glampingsSer = GlampingSerializer(instance=queryset, many=True)
        return Response(glampingsSer.data, status=status.HTTP_200_OK)
    
    def filterByPrice(self, request):
        """
        Retrieves all glampings which price is lower than price_max and/or greater than price_min.
        Returns a queryset object with all the glampings.
        """
        params = dict(request.query_params)
        # Get glampings that match condition
        #TODO hacer tres peticiones diferentes segun los parametros enviados
        queryset = Glamping.objects.filter(price__lte=params['price_max'][0], price__gte=params['price_min'][0])
        return queryset
    
    def filterByIncludes(self, request):
        """
        Retrieves all glampings that includes all the listed elements.
        Returns a queryset object with all the glampings.
        """
        params = dict(request.query_params)
        includes = params['includes'][0].split(",")
        trues = [True]*len(includes)
        queryset = Glamping.objects.filter(includes__contains=dict(zip(includes, trues)))
        return queryset
