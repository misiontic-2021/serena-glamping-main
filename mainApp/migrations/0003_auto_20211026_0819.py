# Generated by Django 3.2.8 on 2021-10-26 13:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0002_auto_20211026_0758'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reserva',
            name='checkin',
            field=models.DateTimeField(verbose_name='Check-in'),
        ),
        migrations.AlterField(
            model_name='reserva',
            name='checkout',
            field=models.DateTimeField(verbose_name='Check-out'),
        ),
        migrations.AlterField(
            model_name='reserva',
            name='glamping',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='glamping_id', to='mainApp.glamping', verbose_name='Glamping'),
        ),
    ]
