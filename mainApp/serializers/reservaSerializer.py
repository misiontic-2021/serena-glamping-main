from rest_framework import serializers

from mainApp.models.reservaModel import Reserva

class ReservaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reserva
        fields = ['user', 'glamping', 'checkin', 'checkout', 'people']