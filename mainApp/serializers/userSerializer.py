from rest_framework import serializers

from mainApp.models.userModel import User

class UserSerializer(serializers.ModelSerializer):

    # Sub-class
    class Meta:
        model = User
        fields = '__all__'