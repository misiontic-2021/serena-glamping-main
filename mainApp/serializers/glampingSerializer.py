from rest_framework import serializers

# Import model
from mainApp.models.glampingModel import Glamping

class GlampingSerializer(serializers.ModelSerializer):

    # Sub-class
    class Meta:
        model = Glamping
        fields = '__all__'
        # fields = ['id', 'name', 'price', 'status', 'includes']
