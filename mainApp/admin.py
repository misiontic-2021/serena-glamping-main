from django.contrib import admin

# Import your models here
from .models.userModel import User
from .models.glampingModel import Glamping
from .models.reservaModel import Reserva

# Register your models here.
admin.site.register(User)
admin.site.register(Glamping)
admin.site.register(Reserva)