from django.db import models
from django.db.models.fields import DateTimeField

from .userModel import User
from .glampingModel import Glamping

class Reserva(models.Model):

    # Choices
    STATUS_CHOICES = [
        ('C', 'Confirmada'),
        ('P', 'Pendiente'),
        ('D', 'Eliminada')
    ]
    
    # Attributes
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, verbose_name="Client", on_delete=models.CASCADE)
    glamping = models.ForeignKey(Glamping, verbose_name="Glamping", related_name='glamping_id', on_delete=models.CASCADE)
    checkin = models.DateTimeField("Check-in", null=False)
    checkout = models.DateTimeField("Check-out", null=False)
    status = models.CharField("Status", max_length=1, choices=STATUS_CHOICES, default='C')
    people = models.SmallIntegerField("Number of people", null=False)
    created_at = models.DateTimeField("Creation date", auto_now_add=True)
    updated_at = models.DateTimeField("Last update date", auto_now_add=True)