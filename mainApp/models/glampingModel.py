from django.db import models

class Glamping(models.Model):

    # Choices
    STATUS_CHOICES = [
        ('A', 'Activo'),
        ('M', 'En mantenimiento'),
        ('C', 'Cerrado')
    ]

    # Attributes
    id = models.AutoField(primary_key=True)
    name = models.CharField("Glamping_name", max_length=25, unique=True)
    price = models.PositiveIntegerField("Price")
    status = models.CharField("Status", max_length=1, choices=STATUS_CHOICES, default='A')
    includes = models.JSONField("Includes")