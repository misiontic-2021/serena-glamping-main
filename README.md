### MISION TIC 2021

# LA SERENA GLAMPING PROJECT

---

## Authors 

|#| Name | Role |
|---|---|---|
| 1 | Camilo Andrés Borda Gil | SCRUM master |
| 2 | Sandra Cristina Espinosa Ramírez | Product Owner |
| 3 | Christian Camilo Bolivar Romero | Developer |
| 4 | Willian Chadid Altamar | Developer |
| 5 | Santiago Echeverria Acosta | Developer |

---

## To start contributing

1. Clone the repository.
2. On root folder, create your own environment and install the frameworks
    > 1. Create new virtual environment with  `python -m venv env`.
    > 2. Activate environment with `env\Scripts\activate`.
    > 3. Install all required libraries with `pip install -r requirements.txt`.
    > 4. Test Django project `python manage.py runserver`.

---

## Create and migrate a new model

1. Define model class on `mainApp/models/[MODEL_NAME].py`.
2. Import model class on `mainApp/models/__init__.py`.
3. Register model on `mainApp/admin.py`.
4. Create serializer for model on `mainApp/serializer/[SERIALIZER_NAME].py`.
5. Import serializer class on `mainApp/serializer/__init__.py`.
6. Generate tables on database:
    - `python manage.py makemigrations [APP_NAME]`.
    - `python manage.py migrate`.

## Add routes and views

1. Create view class on `mainApp/views/[VIEW_NAME].py`.
2. Define all http methods on viewfile (each method must have a response).
3. Import view class on `mainApp/views/__init__.py`.
4. Add URL for created view on `mainProject/urls.py`.
5. Test it!